<?php

namespace App\Http\Controllers;

use App\User;
use App\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    public function index()
    {        
     
        $users = User::all();
            
        return view('users.index', compact('users'));
    }

    public function details($id) {
        $user = User::findOrFail($id);
        $departments = Department::all();
        return view('users.details', compact('user', 'departments'));
    }

    public function changeDepartment($userId, $departmentId) {
        $user = User::findOrFail($userId);
        if (count($user->candidates) > 0) {
            Session::flash('notallowed', 'You are cannot change a department to user that has candidates.');
            return back();
        }
        $user->department_id = $departmentId;
        $user->save();
        return back();
    }

    public function makeManager($userId) {
        $user = User::findOrFail($userId);
        if (Auth::user()->isManager()) {

            $user->roles()->attach(2);
            $user->save();
            return back();
        }
        Session::flash('notallowed', 'Only managers can change role to manager.');
        return back();
    }

    public function removeManager($userId) {
        $user = User::findOrFail($userId);
        if (Auth::user()->isAdmin()) {
            $user->roles()->detach(2);
            $user->save();
            return back();
        }
        Session::flash('notallowed', 'Only Admin can remove role manager.');
        return back();
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        if(Gate::allows('delete-user', $user)) {

            $userToDelete = User::findOrFail($id);
            $userToDelete->delete(); 
        } else {
            Session::flash('notallowed', 'You are not allowed to delete the user becuase you are not an Admin');
        }
        return back();
       
        return redirect('users.index'); 
    }
}
