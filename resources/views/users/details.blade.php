@extends('layouts.app')

@section('title', 'Users')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<h1>User Details</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th>
        <th>Name</th>
        <th>Email</th>
      
        <th>Created</th>
        <th>Department</th>
      
    </tr>
    <!-- the table data -->
    <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
           
                                 
            <td>{{$user->created_at}}</td>
            @if(Auth::user()->isAdmin())
            <td>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     {{$user->department->name}}
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($departments as $department)
                      <a class="dropdown-item" href="{{route('users.changeDepartment',[$user->id, $department->id])}}">{{$department->name}}</a>
                    @endforeach
                    </div>
                  </div>                
            </td>
            @else
                <td>{{$user->department->name}}</td>
            @endif
           
                                                               
        </tr>
</table>
@endsection

