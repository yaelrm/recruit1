@extends('layouts.app')

@section('title', 'Users')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<h1>List of Users</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th>
        <th>Name</th>
        <th>Email</th>
        <!-- <th>Owner</th> -->
        <!-- <th>Status</th> -->
        <th>Created</th>
        <th>Roles</th>
        <th>Delete</th>
        <th>Details</th>
        <th>Make Manager</th>
        @if(Auth::user()->isAdmin())
            <th>Remove Manager Role</th>
        @endif
        <!-- <th>Updated</th> -->
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
          
                                 
            <td>{{$user->created_at}}</td>
            
            <td>
                @foreach($user->roles as $role)
                    <span>{{ $role->name }}</span>
                @endforeach
            </td>
           
            <td>
                <a href = "{{route('users.delete',$user->id)}}">Delete</a>
            </td>   
            <td>
                <a href = "{{route('users.details',$user->id)}}">Details</a>
            </td>  
           
            <td>
             @if (!$user->isManager())
                <a href = "{{route('users.makeManager',$user->id)}}">Make Manager</a>
                @endif      
            </td>
            <td>
                @if(Auth::user()->isAdmin() && $user->isManager())
                <a href = "{{route('users.removeManager',$user->id)}}">Remove Manager Role</a>
                @endif
            </td>
                                                                   
        </tr>
    @endforeach
</table>
@endsection

